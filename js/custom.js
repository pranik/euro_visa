
$(window).resize(function () {
	tableOpen();
})

function tableOpen(){
    $('.pr-f-cell').on('click', function(e){
        e.stopPropagation();
        if(window.innerWidth <= 630){
            clearTimeout(pr_timer);
            pr_this = $(this);
            pr_timer = setTimeout(function(){
                $(pr_this).toggleClass('open');
                $(pr_this).closest('tr').find('.pr-closed').slideToggle(200);
            }, 150)
        }
    })
}


function nmImgFill() {
	if ($(".nm-img-js-wrap").length) {
		var img_count = $(".nm-img-js-wrap").length;
		for (i = 0; i < img_count; i++) {
			var jthis = $(".nm-img-js-wrap").eq(i);
			var jimg = jthis.children("img");
			jimg.removeAttr("style");
			if (jimg.outerWidth() < jthis.outerWidth()) {
				jimg.css("width", "100%").css("height", "auto")
			} else {
				if (jimg.outerHeight() < jthis.outerHeight()) {
					jimg.css("width", "auto").css("height", "100%")
				}
			}
		}
	}
}

//menu toggle
$(".pr-burger").on("click", function (e) {
	e.stopPropagation();
	if (!($(this).hasClass("active"))) {
		$(this).addClass("active");
		$("nav").addClass("show");
		$('body').css('overflow', 'hidden');
	} else {
		$(this).removeClass("active");
		setTimeout(function () {
			$("nav").removeClass("show")
			$('body').css('overflow', 'auto');
		}, 0)
	}
})

//close toggle on body click
$(document).on("click", "*", function (e) {
	e.stopPropagation();
	if (window.innerWidth <= 768) {
		if (!($(this).closest("nav").length)) {
			$('nav').slideUp("fast");
			$(".pr-burger").removeClass("active")
		}
	}
})

/* CONSTRUCTOR */
var li = $('.pr-faq-ul li');
var pr_timer;
var pr_this;

$('.pr-faq-ul li').on('click', function(e){
	e.stopPropagation();
	pr_this = $(this);
	clearTimeout(pr_timer);
	pr_timer = setTimeout(function(){
		pr_this.each(function(){
			if(!(li.hasClass('active'))){
				pr_this.addClass('active');
				pr_this.find('.pr-faq-ans').slideDown();
			} else{
				li.removeClass('active');
				pr_this.addClass('active');
				li.find('.pr-faq-ans').slideUp();
				pr_this.find('.pr-faq-ans').slideDown();
			}
		})
	}, 150)
})

function initMap(){
      var map = new google.maps.Map(document.getElementById('map'),{
       zoom: 15,
       disableDefaultUI: true,
       scrollwheel: false,
       draggable: false,
       disableDoubleClickZoom: true,
       center: {lat: 50.46160772,lng: 30.47091746},
       styles: [{
        stylers: [
         {visibility: "simplified"}
        ] 
       }]
      });
    }

$(document).ready(function () {
	tableOpen();
	initMap();

	$('.section_test a.pr-sbm').on('click', function(){
		clearTimeout(pr_timer);
		pr_timer = setTimeout(function(){
			$('.pr-result-modal').toggleClass('active');
		}, 200)
	});
	$('.pr-result-modal .pr-close').on('click', function(){
		if($('.pr-result-modal').hasClass('active')){
			$('.pr-result-modal').removeClass('active');
		}
	})

	$('.pr-faq-wrap').slick({
		infinite: true,
		arrows: false,
		dots: true
	})
	$('.pr-feedback-slider').slick({
		infinite: true,
		arrows: true,
		dots: true,
		swipe: false,
		slidesToShow: 5,
		slidesToScroll: 5,
		variableWidth: true,
		centerModa: true
	})

})

var nm_count = 0;
var nm_count_last = 0;

$(".js-row input").on("change", function(){
	var label = $(this).closest(".pr-check-wrap").find(".pr-circle");
	nm_count = $(".js-row input:checked").length;

	if (nm_count > nm_count_last) {
		label.html(nm_count);
	}


	if (!$(this).is(":checked")) {
		nm_count--;
		var this_count = parseInt($(this).closest(".pr-check-wrap").find(".pr-circle").html());
		$(".js-row .pr-answer").each(function(){
			var this_label = $(this).closest(".pr-check-wrap").find(".pr-circle");
			var this_counter = parseInt(this_label.html());
			var this_input = $(this).closest(".pr-check-wrap").find("input");
			
			if (parseInt(this_label.html()) > this_count) {
				this_label.html(--this_counter);
				//this_input.prop("checked", false);
				nm_count--;
			}
		})
		label.html("");
	}

	nm_count_last = $(".js-row input:checked").length;
})
